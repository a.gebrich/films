<?php
class Edit
{
    public function readGet($db)
    {
        foreach ($_GET as $k=>$v)
        {
            return $db->FindByColumn($k, $v);
        }
    }

    public function editFilm($db, $sqlResult)
    {
        if(isset($_POST['title']))
        {   
            if (!$this->detectJs($_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
            {
                    if (str_replace(' ', '', $_POST['title']) != '' && $_POST['title'] != $sqlResult[0]['title'])
                    {
                        if(!$this->dublicateFilm($db, 'title', $_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                            $db->updateTitleQuery(htmlspecialchars($_POST['title']), $_GET['id']);
                    }

                    if (str_replace(' ', '', $_POST['year']) != '' && $_POST['year'] != $sqlResult[0]['year'] && $this->yearRange($_POST['year']))
                    {
                        if(!$this->dublicateFilm($db, 'title', $_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                            $db->updateYearQuery(htmlspecialchars($_POST['year']), $_GET['id']);
                    }

                    if (str_replace(' ', '', $_POST['format']) != '' && $this->validateFormat($db))
                    {
                        if(!$this->dublicateFilm($db, 'title', $_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                            $db->updateFormatQuery(htmlspecialchars($_POST['format']), $_GET['id']);
                    } else {
                        echo '<span class="danger">Несуществующий формат</span><br>';
                        die;
                    }

                    if (str_replace(' ', '', $_POST['stars']) != '' && $_POST['stars'] != $sqlResult[0]['stars'])
                    {
                        if(!$this->dublicateFilm($db, 'title', $_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                            $db->updateStarsQuery($this->dublicateStars(htmlspecialchars($_POST['stars'])), $_GET['id']);
                    }
            
                    header("Refresh:0");
            }  
        }

    }

    public function addPhoto($db)
    {
        if(isset($_FILES['photo']))
        {
            $uploads_dir = $_SERVER['DOCUMENT_ROOT'];
            $PhotoPath = $_FILES['photo']['tmp_name'];
            $name = basename($_FILES["photo"]["name"]);

            move_uploaded_file($PhotoPath,  "$uploads_dir/uploads/$name"); 

            $db->updateImageQuery('../uploads/'.$name, $_GET['id']);
            header("Location: ../index.php");
        }
    }

    public function isNotEmpty()
    {
        if (str_replace(' ', '', $_POST['title']) != '' && str_replace(' ', '', $_POST['year']) != '' && str_replace(' ', '', $_POST['format']) != '' && str_replace(' ', '', $_POST['stars']) != '')
            {
                return true;
            } else {
                echo '<span class="danger">Нужно заполнить все поля</span>';
                die;
            }
    }

    public function detectJs(...$whereSearch)
    {
        foreach($whereSearch as $v)
        {
            if(preg_match('/<script>?/', $v))
            {
                echo '<span class="danger">Недопустимые символы</span>'.htmlspecialchars($v);
                return true;
                die;
            } 
        }
    }

    
    public function validateFormat($db)
    {
        $formats = array('DVD', 'Blu-Ray', 'VHS');
        for($i=0; $i < count($formats); $i++)
        {
            if ($_POST['format'] == $formats[$i])
            {
                return true;
            }
        } 
    }

    public function yearRange($year)
    {
        if($year < 1850 || $year > 2021 || is_int($year))
        {
            echo '<span class="danger">Год не соответствует диапазону 1850 - 2021 или не является числом</span>';
            die;
        } else {
            return true;
        }
    }

    public function dublicateFilm($db, $column, $curTitle, $curYear, $curFormat, $curStars)
    {
        if (!empty($db->FindByColumn($column, $curTitle)))
        {
            $result = $db->FindByColumn($column, $curTitle)[0];
        } else {
            return false;
            die;
        }
        
        if($result['title'] == $curTitle && $result['year'] == $curYear && $result['format'] == $curFormat && $result['stars'] == $curStars)
        {
            echo '<span class="danger">Обнаружен дубликат фильма</span>';
            die;
        } else {
            return false;
        }
       
    }

    public function dublicateStars($st)
    {
        $st = str_replace(', ', ',', $st);
        $st = explode(',', $st);
        
            for($i=0;$i < count($st); $i++)
            { 
                if(!$this->validateSymbols($st[$i]))
                {
                    echo '<span class="danger">Неверное имя одного из актеров!</span>';
                    die;
                }   
            }
        $st = array_unique($st);
        return $st = implode(', ', $st);
    }
    
    public function validateSymbols($st)
    {

        if (preg_match('/^(([a-zA-Z\' -]{1,80})|([а-яА-ЯЁёІіЇїҐґЄє\' -]{1,80}))$/u', $st))
        {
            return true;
        } else {
            return false;
        }

    }

    public function addFilm($db)
    {
        if(isset($_POST['title']))
        {   
            if ($this->isNotEmpty())
            {
                if (!$this->detectJs($_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                {
                    if($this->yearRange($_POST['year']))
                    {
                        if($this->validateFormat($db))
                        {
                            if(!$this->dublicateFilm($db, 'title', $_POST['title'], $_POST['year'], $_POST['format'], $_POST['stars']))
                            {
                                $db->insertQuery(htmlspecialchars($_POST['title']), htmlspecialchars($_POST['year']), htmlspecialchars($_POST['format']), $this->dublicateStars(htmlspecialchars($_POST['stars'])));
                                echo '<span class="success">Вы успешно загрузили фильм!</span>';
                            }
                        } else {
                            echo '<span class="danger">Несуществующий формат</span><br>';
                            die;
                        }
                    }
                }
            } 
        } 
    }

    public function addFile($db)
    {
        // If POST exists, checking is there any errors and isn`t size of file zero 
        if ($_POST && !empty($_FILES)) 
        {
            if ($_FILES['userfile']['error'] == 0 && $_FILES['userfile']['size'] != 0)
            {
                
                $FilePath = $_FILES['userfile']['tmp_name'];
                $fp = fopen($FilePath, 'r');
                
                while(!feof($fp)) 
                {
                    
                        $files = fgets($fp);
                        $files = str_replace(array("\r\n", "\r", "\n", "<br>"), '', ($files));
                        
                        if($files == '')
                            continue;
                        
                        if(preg_match('/Title:?\s?/', $files))
                        {
                            $files = preg_replace('/Title:?\s*/', '', $files);
                            $title[] = $files; 
                        } 

                        if(preg_match('/Release?\sYear:?\s*/', $files))
                        {
                            $files = preg_replace('/Release?\sYear:?\s*/', '', $files);
                            $year[] = $files; 
                        }

                        if(preg_match('/Format:?\s/', $files))
                        {
                            $files = preg_replace('/Format:?\s/', '', $files);
                            $format[] = $files; 
                        }

                        if(preg_match('/Stars:?\s/', $files))
                        {
                            $files = preg_replace('/Stars:?\s/', '', $files);
                            $stars[] = $files; 
                        }
                        
                }
                if (count($title) != count($year) || count($title) != count($format) || count($title) != count($stars)) 
                {
                    echo '<span class="success">Некорректный формат файла.</span>';
                    die;
                }
                for ($i=0;$i < count($title);$i++)
                {   //var_dump(empty($db->selectQuery()));die;
                    if(empty($db->selectQuery()) == true)
                    {
                        $db->insertQuery($title[$i], $year[$i], $format[$i], $stars[$i]);
                    } else {
                        
                        if(!$this->dublicateFilm($db, 'title', $title[$i], $year[$i], $format[$i], $stars[$i]))
                        {
                            $db->insertQuery($title[$i], $year[$i], $format[$i], $stars[$i]);
                        } else {
                            continue;
                        }
                    }
                }
                fclose($fp);
                echo '<span class="success">Вы успешно загрузили файл!</span>';

            } else if ($_FILES['userfile']['size'] == 0){
                echo '<span class="danger">Файл пуст...</span>';
            } else if ($_FILES['userfile']['error'] == 4){
                echo '<span class="danger">Нет файла</span>';
            } else {
                echo '<span class="danger">Error</span>';
            }
        }
    }

    public function delete($db, $whichDelete)
    {
        if(isset($_POST['delete']))
        {
            $db->deleteQuery($whichDelete);
            header("Location: ../index.php");
        }
    }
}