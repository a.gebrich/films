<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class User extends PDO
{
    private $dbname = 'films';
    private $host = 'your_host';
    private $user = 'user';
    private $pass = 'pass';
    private $driver = 'mysql';
    private $username = NULL;
    private $password = NULL;
    private $password_repeat = NULL;
    

    public function __construct($username, $password = NULL, $password_repeat = NULL)
    {
        $connect = $this->driver . ':host=' . $this->host . ';dbname=' . $this->dbname;
        parent::__construct($connect, $this->user, $this->pass);
        $this->username = $username;
        $this->password = $password;
        $this->password_repeat = $password_repeat;
    }

    public function register()
    {
        $sql = "INSERT INTO `users` (`username`, `password`) VALUES (?, ?);";
        $result = $this->prepare($sql);
        return $articles = $result->execute([$this->username, $this->protectPas()]);
    }

    public function issetUsername()
    {
        $sql = "SELECT * FROM `users` WHERE `username` = ?";
        $result = $this->prepare($sql);
        $result->execute([$this->username]);
        return $result->fetchAll(PDO::FETCH_ASSOC);

    }

    public function passLen($limit)
    {
        if (strlen($this->password) > $limit) {
            return true;
        }   else    {
            return false;
        }
    }

    public function checkPass()
    {
        if($this->password === $this->password_repeat)
        {
            return true;
        } else {
            return false;
        }
    }

    public function login(){
        session_start();
        $_SESSION['login'] = $this->username;
        $_SESSION['status'] = $this->issetUsername()[0]['status'];
    }

    public function logout(){
        session_destroy();
    }

    public function protectPas()
    {
        return md5($this->password);
    }
}