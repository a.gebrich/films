<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class dataBasePDO extends PDO
{
    private $dbname = 'films';
    private $host = 'your_host';
    private $user = 'user';
    private $pass = 'pass';
    private $driver = 'mysql';
    

    public function __construct()
    {
        $connect = $this->driver . ':host=' . $this->host . ';dbname=' . $this->dbname;
        parent::__construct($connect, $this->user, $this->pass);
    }
    
    public function selectQuery()
    {
        $sql = "SELECT * FROM `films` ORDER BY `id` DESC";
        $result = $this->query($sql);
        if ($result != false) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return null;
        }
    }

    public function FindByColumn($column, $value)
    {
        $sql = "SELECT * FROM `films` WHERE `$column` = '$value';";
        $result = $this->query($sql);
        if ($result != false) {
            return $articles = $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return null;
        }
    }

    public function updateTitleQuery($title, $id)
    {
        $sql = "UPDATE `films` SET title=? WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $articles = $result->execute([$title, $id]);
        } else {
            return null;
        } 
    }

    public function updateYearQuery($year, $id)
    {
        $sql = "UPDATE `films` SET year=? WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $articles = $result->execute([$year, $id]);
        } else {
            return null;
        } 
    }
    
    public function updateFormatQuery($format, $id)
    {
        $sql = "UPDATE `films` SET format=? WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $articles = $result->execute([$format, $id]);
        } else {
            return null;
        }
    }

    public function updateStarsQuery($stars, $id)
    {
        $sql = "UPDATE `films` SET stars=? WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $result->execute([$stars, $id]);
        } else {
            return null;
        }
    }

    public function updateImageQuery($imagePath, $id)
    {
        $sql = "UPDATE `films` SET image_path=? WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $result->execute([$imagePath, $id]);
        } else {
            return null;
        }
    }

    public function deleteQuery($id)
    {
        $sql = "DELETE FROM `films` WHERE id=?";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $result->execute([$id]);
        } else {
            return null;
        }
    }

    public function selectQueryGroupBy($column)
    {
        $sql = "SELECT * FROM `films` GROUP BY $column";
        $result = $this->query($sql);
        if ($result != false) {
            return $articles = $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return null;
        }
        
    }

    public function searchQuery($column, $search)
    {
        $sql = "SELECT * FROM `films` WHERE $column LIKE ?;";
        $result = $this->prepare($sql);
        $result->execute(['%'.$search.'%']);
        if ($result != false) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return null;
        }
    }

    public function insertQuery($title, $year, $format, $stars)
    {
        $sql = "INSERT INTO `films` (`title`, `year`, `format`,`stars`) VALUES (?, ?, ?, ?);";
        $result = $this->prepare($sql);
        if ($result != false) {
            return $result->execute([$title, $year, $format, $stars]);
        } else {
            return null;
        }
    }
}
?>