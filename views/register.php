<?php
session_start();
$pageTitle = 'Регистрация';
$styles ='../style/style.css';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes/pdo.php');
require_once('../classes/user.php');
require_once('../classes/edit.php');

$checkU = new Edit;

if (isset($_POST['username']))
{
    if ($_POST['username'] != '' && $_POST['password'] != '' && $_POST['password_repeat'] != '')
    {    

        $reg = new User($_POST['username'], trim($_POST['password']), trim($_POST['password_repeat']));

        if($reg->passLen(3))
        {
                if(!$reg->issetUsername())
                {
                    if(!$checkU->detectJs($_POST['username']))
                    {
                        switch ($reg->checkPass())
                        {
                            case true:
                                $reg->register();
                                header('Location: ./signin.php');
                                break;
                            case false:
                                echo '<span class="danger">Пароли не совпадают</span>';
                                break;
                        }
                    }
                }
                else {
                        echo '<span class="danger">Логин уже используется...</span>';
                    }
        } else {
            echo '<span class="danger">Пароль должен быть более 3 символов!</span>';
        }
    }    else {
        echo '<span class="danger">Заполните все поля</span>';
    }
}
?>

<div class="container">
    <form class="register_form" action="" method="post">
        <input type="text" name="username" placeholder="Логин">
        <input name="password" type="password" placeholder="Пароль">
        <input name="password_repeat" type="password" placeholder="Повторите пароль">
        <input type="submit" value='Регистрация!'>
    </form>
</div>