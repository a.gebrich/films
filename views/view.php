<?php
session_start();

$pageTitle = 'Info';
$styles ='../style/style.css';
$pathViews = '';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes//pdo.php');

$db = new dataBasePDO;

foreach ($_GET as $k=>$v)
{
    $sqlResult = $db->FindByColumn($k, $v);
}

?>

<div class="container">
    <div class="view_content">
        <div class="imageFilm">
            <img src="<?= ($sqlResult[0]['image_path'] != NULL) ? $sqlResult[0]['image_path'] : '../uploads/unknown.jpg'?>"/>
        </div>
        <div class="title"><?=$sqlResult[0]['title']?></div>
        <div class="year"><?=$sqlResult[0]['year']?></div>
        <div class="format"><?=$sqlResult[0]['format']?></div>
        <div class="stars"><?=$sqlResult[0]['stars']?></div>
    </div>
</div>