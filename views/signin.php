<?php
$pageTitle = 'Вход';
$styles ='../style/style.css';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes/pdo.php');
require_once('../classes/user.php');


if (isset($_POST['username']))
{
    if ($_POST['username'] != '' && $_POST['password'] != '')
    {    
        $sign = new User($_POST['username'], trim($_POST['password']));
            if($sign->issetUsername())
            {
                    if($sign->protectPas() == $sign->issetUsername()[0]['password'])
                    {
                        $sign->login();
                        header('Location: ../index.php');
                    } else 
                    {
                        echo '<span class="danger">Неверный пароль</span>';
                    }
            }   else {
                 echo '<span class="danger">Логина не найдено</span>';
            }

    } else {
        echo '<span class="danger">Нужно заполнить все поля</span>';
    }
}


?>
<div class="container">
    <form class="signin_form" action="" method="post">
        <input type="text" name="username" placeholder="Логин">
        <input name="password" type="password" placeholder="Пароль">
        <input type="submit" value='Войти'>
    </form>
</div>