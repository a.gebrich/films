<?php
session_start();
if(isset($_SESSION['status']) && $_SESSION['status'] != 'Admin')
    header('Location: ../index.php');

$pageTitle = 'Добавить фильм';
$styles ='../style/style.css';
$pathViews = '';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes/pdo.php');
require_once('../classes/edit.php');

$formats = array('DVD', 'Blu-Ray', 'VHS');

$db = new dataBasePDO;

$add = new Edit;

$add->addFilm($db);

$add->addFile($db);
?>

<div class="container">
    <form action="" name="create_form" class="change_form" method="POST">
        <input type="text" name="title" placeholder="Название">
        <input type="text" name="year" placeholder="Год сьемки">
        <label for="format"> Формат
            <select name="format">
                <?php
                $i=0;
                 for($i=0; $i < count($formats); $i++)
                {
                    echo '<option>'.$formats[$i].'</option>';
                }
                ?>
                </select>
        </label>
        <textarea name="stars" id="" rows="5" placeholder="Имена актеров через запятую"></textarea>
        <input type="submit" value="Добавить фильм" style="background-color:greenyellow;">
    </form>

    <form enctype="multipart/form-data" action="" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
    <label for=""> Либо импортируйте файл 
        <input required type="file" name="userfile" >
    </label>
    <input required type="submit" value="Импорт" style="background-color:skyblue;">
</form>