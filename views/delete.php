<?php
session_start();
if(isset($_SESSION['status']) && $_SESSION['status'] != 'Admin')
    header('Location: ../index.php');

$pageTitle = 'Удаление фильмов';
$styles ='../style/style.css';
$pathViews = '';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes/pdo.php');
require_once('../classes/edit.php');

$db = new dataBasePDO;

$add = new Edit;
if(isset($_POST))
{
    foreach($_POST as $k=>$v)
    {
        if(preg_match('/Удалить фильмы?/', $v))
        {
            continue;
        } else {
            $add->delete($db, $v);
        } 
    }
}
?>

<div class="container">
    <form action="" id="delete_form" name="delete_form" method="POST">
        <div class="items">
            <?php
                for ($i=0;$i < count($db->selectQuery());$i++)
                {?>
                    <div class="item">
                        <?php echo '<input class="checkboxesDelete" type="checkbox" name="'.$db->selectQuery()[$i]['id'].'" value="'.$db->selectQuery()[$i]['id'].'"><br>';?>
                            <a><img src="<?= ($db->selectQuery()[$i]['image_path'] != NULL) ? $db->selectQuery()[$i]['image_path'] : '../uploads/unknown.jpg'?>"></a>
                            <div class="name"><?=$db->selectQuery()[$i]['title']?></div>
                            <div class="year"><?=$db->selectQuery()[$i]['year']?></div>
                    </div>
                    
        <?php   } ?> 
        </div>
        <input type="submit" name="delete" value="Удалить фильмы"  style="position: fixed;right: 50px; top: 90%; padding: 20px;" class="delete_film">
    </form>
</div>