<?php
session_start();
if(isset($_SESSION['status']) && $_SESSION['status'] != 'Admin')
    header('Location: ../index.php');
    
$pageTitle = 'Редактирование';
$styles ='../style/style.css';
$pathViews = '';
$pathJS = '..';
require_once('../components/head.php');
require_once('../classes/pdo.php');
require_once('../classes/edit.php');

$formats = array('DVD', 'Blu-Ray', 'VHS');

$db = new dataBasePDO;

$Film = new Edit;

$sqlResult = $Film->readGet($db);

$Film->editFilm($db, $sqlResult);

$Film->addPhoto($db);

$Film->delete($db, $_GET['id']);
?>

<div class="container">
    <form action="" name="change_form" class="change_form" method="POST">
        <input type="text" name="title" value="<?=$sqlResult[0]['title']?>">
        <input type="text" name="year" value="<?=$sqlResult[0]['year']?>">
        <label for="format"> Формат
            <select name="format">
                <option><?=$sqlResult[0]['format']?></option>
                <?php
                $i=0;
                 for($i=0; $i < count($formats); $i++)
                {
                    if($formats[$i] != $sqlResult[0]['format'])
                    {
                        echo '<option>'.$formats[$i].'</option>';
                    } else {
                        continue;
                    }
                }
                ?>
                </select>
        </label>
        <textarea name="stars" id="" rows="5" placeholder="<?=$sqlResult[0]['stars']?>"></textarea>
        <input type="submit" value="Редактировать" style="background-color:greenyellow;">
    </form>

    <form action="" id="delete_form" name="delete_form" method="POST">
        <input type="submit" name="delete" value="Удалить фильм"  class="delete_film">
    </form>

    <form enctype="multipart/form-data" action="" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
    <label for=""> Обложка 
        <input required type="file" name="photo" >
    </label>
    <input required type="submit" value="Отправить" >
</div>
