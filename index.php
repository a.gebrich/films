<?php
session_start();
    
$pageTitle = 'Items';
$pathViews = './views/';
$styles ='./style/style.css';
$pathJS = '.';
require_once('./components/head.php');
require_once('./classes/pdo.php');

$db = new dataBasePDO;

if (isset($_POST['groupBy']))
{
    setcookie('groupBy', 1, time()+3600);
    header("Refresh:0");
} else if(isset($_POST['default']))
{
    setcookie('groupBy', 0, time()+3600);
    header("Refresh:0");
}
?>

<div class="container">
<h2>Items</h2>
    <form action="" method="post">
        <input type="<?=(!isset($_GET['search_title'])) && (!isset($_GET['search_star'])) ? 'submit' : 'hidden'?>" name="<?=(!isset($_COOKIE['groupBy']) || $_COOKIE['groupBy'] == 0) ? 'groupBy' : 'default'?>" value="<?=(!isset($_COOKIE['groupBy']) || $_COOKIE['groupBy'] == 0) ? 'По алфавиту' : 'Самые новые'?>">
    </form>
    <form action="" method="get">
        <input type="text" name="search_title" placeholder="По названию">
        <input type="text" name="search_star" placeholder="По актеру">
        <input type="submit" value='Искать!'>
    </form>
    <div class="items">
        <?php 
        if (!isset($_COOKIE['groupBy']) && !isset($_GET['search_title']) && !isset($_GET['search_star']) || isset($_COOKIE['groupBy']) && $_COOKIE['groupBy'] == 0 && !isset($_GET['search_title']) && !isset($_GET['search_star']) )
        {
            for ($i=0;$i < count($db->selectQuery());$i++)
            {?>
                <div class="item">
                <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                    <a href="<?='./views/view.php?id='.($db->selectQuery()[$i]['id'])?>"><img src="<?= ($db->selectQuery()[$i]['image_path'] != NULL) ? $db->selectQuery()[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                    <div class="name"><?=$db->selectQuery()[$i]['title']?></div>
                    <div class="year"><?=$db->selectQuery()[$i]['year']?></div>
                </div>
        <?php }
        } else if(isset($_COOKIE['groupBy']) && $_COOKIE['groupBy'] == 1  && !isset($_GET['search_title']) && !isset($_GET['search_star']) )
        {
            for ($i=0;$i < count($db->selectQueryGroupBy('title'));$i++)
            {?>  
                <div class="item">
                <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQueryGroupBy('title')[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                    <a href="<?='./views/view.php?id='.($db->selectQueryGroupBy('title')[$i]['id'])?>"><img src="<?= ($db->selectQueryGroupBy('title')[$i]['image_path'] != NULL) ? $db->selectQueryGroupBy('title')[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                    <div class="name"><?=$db->selectQueryGroupBy('title')[$i]['title']?></div>
                    <div class="year"><?=$db->selectQueryGroupBy('title')[$i]['year']?></div>
                </div>
        <?php }
        }
        if (isset($_GET['search_title']) && $_GET['search_title'] != '' && isset($_GET['search_star']) && $_GET['search_star'] != '')
        {
            for ($i=0;$i < count($db->searchQuery('title', $_GET['search_title']));$i++)
            {
                $compareTitle = ($db->searchQuery('title', $_GET['search_title'])[$i]['title'] == $db->searchQuery('stars', $_GET['search_star'])[$i]['title']);
                $compareYear = ($db->searchQuery('title', $_GET['search_title'])[$i]['year'] == $db->searchQuery('stars', $_GET['search_star'])[$i]['year']);
                $compareFormat = ($db->searchQuery('title', $_GET['search_title'])[$i]['format'] == $db->searchQuery('stars', $_GET['search_star'])[$i]['format']);
                $compareStars = ($db->searchQuery('title', $_GET['search_title'])[$i]['stars'] == $db->searchQuery('stars', $_GET['search_star'])[$i]['stars']);

                if($compareTitle && $compareYear && $compareFormat && $compareStars)
                {?>
                <div class="item">
                <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                    <a href="<?='./views/view.php?id='.($db->searchQuery('title', $_GET['search_title'])[$i]['id'])?>"><img src="<?= ($db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] != NULL) ? $db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                    <div class="name"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['title']?></div>
                    <div class="year"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['year']?></div>
                </div>
        <?php   } else { ?>
                                    <div class="item">
                                    <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                                        <a href="<?='./views/view.php?id='.($db->searchQuery('title', $_GET['search_title'])[$i]['id'])?>"><img src="<?= ($db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] != NULL) ? $db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                                        <div class="name"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['title']?></div>
                                        <div class="year"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['year']?></div>
                                    </div>

                                    <div class="item">
                                    <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                                        <a href="<?='./views/view.php?id='.($db->searchQuery('stars', $_GET['search_star'])[$i]['id'])?>"><img src="<?= ($db->searchQuery('stars', $_GET['search_star'])[$i]['image_path'] != NULL) ? $db->searchQuery('stars', $_GET['search_star'])[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                                        <div class="name"><?=$db->searchQuery('stars', $_GET['search_star'])[$i]['title']?></div>
                                        <div class="year"><?=$db->searchQuery('stars', $_GET['search_star'])[$i]['year']?></div>
                                    </div>
                            <?php 
                            
                        }
            }
        } else if (isset($_GET['search_title']) && $_GET['search_title'] != '' && isset($_GET['search_star']) && $_GET['search_star'] == '')
        {
            for ($i=0;$i < count($db->searchQuery('title', $_GET['search_title']));$i++)
            {?>
                <div class="item">
                <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                    <a href="<?='./views/view.php?id='.($db->searchQuery('title', $_GET['search_title'])[$i]['id'])?>"><img src="<?= ($db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] != NULL) ? $db->searchQuery('title', $_GET['search_title'])[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                    <div class="name"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['title']?></div>
                    <div class="year"><?=$db->searchQuery('title', $_GET['search_title'])[$i]['year']?></div>
                </div>
        <?php }
        } else if (isset($_GET['search_star']) && $_GET['search_star'] != '' && isset($_GET['search_title']) && $_GET['search_title'] == '')
        {
            for ($i=0;$i < count($db->searchQuery('stars', $_GET['search_star']));$i++)
            {?>
                <div class="item">
                <?php echo (isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<a href="./views/change.php?id='.($db->selectQuery()[$i]['id']).'" class="change">Редактировать</a>' : ''?>
                    <a href="<?='./views/view.php?id='.($db->searchQuery('stars', $_GET['search_star'])[$i]['id'])?>"><img src="<?= ($db->searchQuery('stars', $_GET['search_star'])[$i]['image_path'] != NULL) ? $db->searchQuery('stars', $_GET['search_star'])[$i]['image_path'] : './uploads/unknown.jpg'?>"></a>
                    <div class="name"><?=$db->searchQuery('stars', $_GET['search_star'])[$i]['title']?></div>
                    <div class="year"><?=$db->searchQuery('stars', $_GET['search_star'])[$i]['year']?></div>
                </div>
        <?php }
        }?>
         
    </div>
</div>

</body>
</html>

