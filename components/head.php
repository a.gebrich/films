<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <title><?=$pageTitle?></title>
    <link rel="stylesheet" href="<?=$styles?>"/>
    <script rel="text/javascript" src="<?=$pathJS?>/script/jquery-3.5.1.min.js"></script>
    <script rel="text/javascript" src="<?=$pathJS?>/script/script.js"></script>
</head>
<header class="header">
                <div class="container_header">
                    <div class="header_body">
                        <a class="#" href="index.php">
                            <img src="#" alt=''>
                        </a>
                        <div class="header_burger">
                            <span></span>
                        </div>
                        <nav class="header_menu">
                            <ul class="header_list">
                                <li>
                                    <a href="<?='https://'.$_SERVER['SERVER_NAME']?>" class="header_link">Главная</a>
                                </li>
                            <?=(isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<li><a href="'.$pathViews.'create.php" class="header_link">Добавить фильм</a></li>' : '';?>

                            <?=(isset($_SESSION['status']) && $_SESSION['status'] == 'Admin') ? '<li><a href="'.$pathViews.'delete.php" class="header_link">Удаление фильмов</a></li>' : '';?>
                                
                            <?=(!isset($_SESSION['login'])) ? '<li><a href="'.$pathViews.'signin.php" class="header_link">Вход</a></li>' : '';?>
                                
                            <?=(!isset($_SESSION['login'])) ? '<li><a href="'.$pathViews.'register.php" class="header_link">Регистрация</a></li>' : '';?>

                            <?=(isset($_SESSION['login'])) ? '<li><a href="'.$pathViews.'exit.php" class="header_link">Выйти</a></li>' : '';?>

                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
<body>