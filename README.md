#FILMS

Залиты правки по фидбеку от 30.08

Для запуска проекта необходимо склонировать ветку в локальный репозиторий.
Для работы необходимо соблюдение требований PDO. https://www.php.net/manual/ru/book.pdo.php
В классах, наследующих PDO (/classes/pdo.php, /classes/user.php) необходимо изменить приватные свойства на актуальные для сервера.

Работоспособность проекта проверена на сервере с програмным обеспечением Ubuntu 20.10, php7.4, nginx 1.18.0, mariaDB 10.3.29 (MySql)

Подразумевается что upload_tmp_dir будет находиться в корневой директории DOCUMENT_ROOT. Указать его можно в конфиге php.ini , либо в сокете доменного имени php-fpm при использовании nginx по пути /etc/php/{version}/fpm/pool.d/{nameofhost.com}

При переходе на сайт гостю показан список фильмов, где отображается обложка фильма (если она была загружена администраторм), название фильма и год его выпуска. Посетитель может перейти во вкладку с детальной информацией, где дополнительно будет указан основной актерский состав и формат записи.

Посетитель имеет возможность сортировать записи по алфавиту, а так же по новизне (default). 
Есть возможность осуществлять поиск как по названию фильма, так и по имени актера.
Гостю предоставлена возможность зарегестрироваться на сайте либо осуществить авторизацию.

Предусмотрена отдельная категория пользователей с категорией "Администратор".
Администратор может добавить запись о фильме на сайт, перейдя во вкладку "Добавить фильм" и заполнив поля с описанием. В данной вкаладке также предусмотрена возможность импортировать свой файл с записями в формате:


```
Title: Blazing Saddles
Release Year: 1974
Format: VHS
Stars: Mel Brooks, Clevon Little, Harvey Korman, Gene Wilder, Slim Pickens, Madeline Kahn


Title: Casablanca
Release Year: 1942
Format: DVD
Stars: Humphrey Bogart, Ingrid Bergman, Claude Rains, Peter Lorre
```

Количество ключей "Title:", "Release Year:", "Format:", "Stars:" должно совпадать. Переводы строки допускаются.

Администратору доступна возможность редактирования "фильмов". Для этого ему необходимо перейти во вкладку "Редактировать", находящуюся на обложке фильма и видимой только администратору. Там же он может добавить обложку для фильма. Рекомендуемый форма обложки jpeg/jpg. В данной вкладке есть возможность удалить запись о фильме. Возможность доступна только администратору.

Появилсь вкладка "Удаление фильмов", где администратору можно удалить фильмы, выбрав их при помощи checkbox.

Сделать пользователя администратором можно через панель sql, обновив значение столбца "status" на "Admin" для его учетной записи.
В будущем возможно добавление предоставления привелегий администратора через веб-интерфейс другим админом.

После создания баз доступно две учетные записи: 
admin:admin - администратор
user:user - пользователь

Запросы создания необходимых таблиц:

```
CREATE TABLE `films` (
	`id` INT(7) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(200) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`year` INT(4) NULL DEFAULT NULL,
	`format` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	`stars` VARCHAR(400) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	`image_path` VARCHAR(300) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`time` DATETIME NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_unicode_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
CREATE TABLE `users` (
	`id` INT(7) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(30) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	`password` VARCHAR(100) NOT NULL DEFAULT '' COLLATE 'utf8_unicode_ci',
	`status` VARCHAR(30) NULL DEFAULT 'User' COLLATE 'utf8_unicode_ci',
	`time` DATETIME NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8_unicode_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
INSERT INTO `users` (`username`, `password`, `status`) VALUES ('admin','21232f297a57a5a743894a0e4a801fc3', 'Admin');
INSERT INTO `users` (`username`, `password`) VALUES ('user','ee11cbb19052e40b07aac0ca060c23ee');
```

Рекомендую добавить

```
INSERT INTO `films` (`title`, `year`, `format`, `stars`) VALUES ('Iron Man', '2008', 'DVD', 'Robert Downey Jr, Terrence Howard, Jeff Bridges, Gwyneth Paltrow');
```
